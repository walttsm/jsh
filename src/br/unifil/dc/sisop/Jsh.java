package br.unifil.dc.sisop;

import java.io.*;
import java.nio.file.Path;
import java.util.Optional;

/**
 * Write a description of class Jsh here.
 *
 * @author Ricardo Inacio Alvares e Silva
 * @version 180823
 */
public final class Jsh {

    /**
     * Funcao principal do Jsh.
     */
    public static void promptTerminal() {
        System.setProperty("user.dir", System.getProperty("user.home"));

        while (true) {
            try {
                exibirPrompt();
                ComandoPrompt comandoEntrado = lerComando();
                executarComando(comandoEntrado);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * Escreve o prompt na saida padrao para o usuário reconhecê-lo e saber que o
     * terminal está pronto para receber o próximo comando como entrada.
     */
    public static void exibirPrompt() throws IOException {
        String userName = System.getProperty("user.name");
        Process uidProcess = Runtime.getRuntime().exec("id -u " + userName);
        InputStream in = uidProcess.getInputStream();
        String uid = String.valueOf(in.read());
        System.out.print(userName + "#" + uid + ":" + System.getProperty("user.dir") + "% ");

        //throw new RuntimeException("Método ainda não implementado.");
    }

    /**
     * Preenche as strings comando e parametros com a entrada do usuario do terminal.
     * A primeira palavra digitada eh sempre o nome do comando desejado. Quaisquer
     * outras palavras subsequentes sao parametros para o comando. A palavras sao
     * separadas pelo caractere de espaco ' '. A leitura de um comando eh feita ate
     * que o usuario pressione a tecla <ENTER>, ou seja, ate que seja lido o caractere
     * EOL (End Of Line).
     *
     * @return
     */
    public static ComandoPrompt lerComando() throws IOException {
        BufferedReader terminal = new BufferedReader(new InputStreamReader(System.in));
        ComandoPrompt prompt = new ComandoPrompt(terminal.readLine());

        return prompt;
        //throw new RuntimeException("Método ainda não implementado.");
    }

    /**
     * Recebe o comando lido e os parametros, verifica se eh um comando interno e,
     * se for, o executa.
     * <p>
     * Se nao for, verifica se é o nome de um programa terceiro localizado no atual
     * diretorio de trabalho. Se for, cria um novo processo e o executa. Enquanto
     * esse processo executa, o processo do uniterm deve permanecer em espera.
     * <p>
     * Se nao for nenhuma das situacoes anteriores, exibe uma mensagem de comando ou
     * programa desconhecido.
     */
    public static void executarComando(ComandoPrompt comando) {
        int statusCode = 0;
        switch (comando.getNome()) {
            case "encerrar":
                System.out.println("Programa encerrado com statusCode " + statusCode);
                System.exit(statusCode);
            case "relogio":
                statusCode = ComandosInternos.exibirRelogio();
                return;
            case "la":
                statusCode = ComandosInternos.escreverListaArquivos(Optional.of(System.getProperty("user.dir")));
                return;
            case "cd":
                statusCode = ComandosInternos.criarNovoDiretorio(comando.getArgumentos().get(0));
                return;
            case "ad":
                statusCode = ComandosInternos.apagarDiretorio(comando.getArgumentos().get(0));
                return;
            case "mdt":
                statusCode = ComandosInternos.mudarDiretorioTrabalho(comando.getArgumentos().get(0));
                return;
            default:
                statusCode = executarPrograma(comando);

        }
        if (statusCode != 0) {
            System.out.println("Erro: o programa indicou termino com falha!");
        }
    }

    public static int executarPrograma(ComandoPrompt comando) {
        File dir = new File(System.getProperty("user.dir"));
        File[] files = dir.listFiles();
        File program = null;
        int ret = 0;

        for (File file : files) {
            if (file.getName().equals(comando.getNome())) {
                program = file;
                break;
            }
        }

        if (program == null) {
            System.out.println("Comando ou arquivo inexistente.");
        } else {
            if (program.canExecute()) {
                ProcessBuilder pcb = new ProcessBuilder(program.getAbsolutePath());
                try {
                    Process process = pcb.start();
                    BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
                    System.out.println(in.readLine());
                    process.waitFor();
                    ret = process.exitValue();
                } catch (IOException e) {
                    System.out.println("Falha ao executar o programa!");
                    e.printStackTrace();
                    return 1;
                } catch (InterruptedException e) {
                    System.out.println("Execução do programa interrompida");
                }
            } else {
                System.out.println(program.getName() + " não possui permissão para execução.");
            }

        }

        return ret;
        //throw new RuntimeException("Método ainda não implementado.");
    }



    /**
     * Entrada do programa. Provavelmente você não precisará modificar esse método.
     */
    public static void main(String[] args) {

        promptTerminal();
    }


    /**
     * Essa classe não deve ser instanciada.
     */
    private Jsh() {
    }

    public static Path workDir;
}
