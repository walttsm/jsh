package br.unifil.dc.sisop;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

/**
 * Write a description of class ComandosInternos here.
 *
 * @author Ricardo Inacio Alvares e Silva
 * @version 180823
 */
public final class ComandosInternos {
    
    public static int exibirRelogio() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        String time = ("A data e hora atual do sistema são: " + dtf.format(LocalDateTime.now()));
        System.out.println(time);
        return 0;
        //throw new RuntimeException("Método ainda não implementado");
    }
    
    public static int escreverListaArquivos(Optional<String> nomeDir) {
        File dir =  new File(nomeDir.get());
        File[] files = dir.listFiles();

        for (File file : files) {
            System.out.println(file.getName());
        }

        return 0;
        //throw new RuntimeException("Método ainda não implementado");
    }
    
    public static int criarNovoDiretorio(String nomeDir) {
        String filepath = System.getProperty("user.dir") + "/" + nomeDir;
        File newdir = new File(filepath);
        boolean result = newdir.mkdir();
        if (!result) {
            System.out.println("Arquivo/Diretório já existente");
            return 1;
        }
        return 0;

        //throw new RuntimeException("Método ainda não implementado");
    }
    
    public static int apagarDiretorio(String nomeDir) {
        String filepath = nomeDir.contains("/") ? nomeDir : System.getProperty("user.dir") + "/" + nomeDir;
        File newdir = new File(filepath);
        boolean result = newdir.delete();
        if (!result) {
            System.out.println("Não foi possível apagar o diretório, verifique se ele existe.");
            return 1;
        }
        return 0;
        //throw new RuntimeException("Método ainda não implementado");
    }
    
    public static int mudarDiretorioTrabalho(String nomeDir){
        String currentPath = System.getProperty("user.dir");
        String path;
        if (nomeDir.equals("..")) {
            path = currentPath.substring(0, currentPath.lastIndexOf("/"));
        } else if (!nomeDir.contains("/")){
            path = currentPath + "/" + nomeDir;
        } else {
            path = nomeDir;
        }
        File file = new File (path);
        if (!file.exists()) {
            System.out.println("Arquivo/Diretório inexistente.");
            return 1;
        }
        System.setProperty("user.dir", path);
        return 0;
        //throw new RuntimeException("Método ainda não implementado");
    }
    
    /**
     * Essa classe não deve ser instanciada.
     */
    private ComandosInternos() {}
}
